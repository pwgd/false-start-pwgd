Feature: User basics

  @api
  Scenario: A logged in user can see her own page
    Given I am logged in as a user with the "authenticated user" role
    When I click "My account"
    Then I should see the heading "Member for"

  @api
  Scenario: The default content editor user can log in
    Given I go to "/user/login"
    When I fill in "Username" with "josh"
    And I fill in "Password" with "j"
    And I press "Log in"
    Then the response status code should be 200
    And I should see the link "Shortcuts" in the "toolbar" region
    And I should see the link "Content" in the "toolbar" region
    And I should not see the link "Extend" in the "toolbar" region

  @api
  Scenario: Site administrators can create a content editor user account
    Given I am logged in as a user with the "site_administrator" role
    When I click "People" in the "toolbar" region
    And I click "Add user"
    Then I should see the text "Roles"
    And I should see the text "Content editor"
    When I fill in "Email address" with "ce@example.com"
    And I fill in "Username" with "ce"
    And I fill in "Password" with "shouldnthavetodothis"
    And I fill in "Confirm password" with "shouldnthavetodothis"
    And I check the box "Content editor"
    And press "Create new account"
    Then the response status code should be 200
    And I should see the success message "Created a new user account for"

  Scenario: A content editor can log in on an access denied page
    Given I go to "/node/add/page"
    When the response status code should be 403
    Then I should see the heading "Log in"
