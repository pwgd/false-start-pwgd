Feature: Page content

  @api
  Scenario: A content editor can post and edit Page content
    Given I am logged in as a user with the "content_editor" role
    When I go to "/node/add/page"
    Then the response status code should be 200
    When I fill in "Title" with "Purpose"
    And I press the "Save and publish" button
    Then the response status code should be 200
    And I should see the heading "Purpose"

    # Given the above
    When I click "Edit"
    Then the response status code should be 200
    When I fill in "Title" with "Raison d'être"
    And I press the "Save and keep published" button
    Then the response status code should be 200
    And I should see "Raison d'être"

  @api
  Scenario: A content editor can add a page to the main menu
    Given I am logged in as a user with the "content_editor" role
    And I am viewing a "page":
    |title|Jolly Good Show|
    And the response status code should be 200
    And I see the heading "Jolly Good Show"
    When I click "Edit"
    Then the response status code should be 200
    When I check the box "Provide a menu link"
    And I fill in "Menu link title" with "Splendiferous"
    And I select "<Main navigation>" from "Parent item"
    And I press the "Save and keep published" button
    Then the response status code should be 200

    Given I am on the homepage
    Then I should see the link "Splendiferous"
    When I click "Splendiferous"
    Then the response status code should be 200
    And I should see the heading "Jolly Good Show"

  @api
  Scenario: A visitor can see a page
    Given I am viewing a "page":
      |title|The why-tos and here-to-fors|
    Then I see the heading "The why-tos and here-to-fors"
