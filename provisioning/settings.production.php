<?php

/**
 * @file
 * Settings for the production environment.
 */

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => 'drupal',
  'username' => 'root',
  'password' => '',
  'host' => 'localhost',
  'prefix' => '',
);

$settings['trusted_host_patterns'] = array(
  'pwgd.com',
  'direct.pwgd.com',
);

