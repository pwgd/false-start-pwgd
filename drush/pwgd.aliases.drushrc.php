<?php

$aliases['dev'] = array(
  'root' => '/vagrant/web',
  'uri' => 'http://pwgd.local',
);

$aliases['stage'] = array(
  'root' => '',
  'uri' => '',
  'remote-user' => 'root',
  'remote-host' => '',
  'path-aliases' => array(
    '%drush' => '',
    '%drush-script' => '',
  ),
);

$aliases['production'] = array(
  'root' => '',
  'uri' => '',
  'remote-user' => 'root',
  'remote-host' => '',
  'path-aliases' => array(
    '%drush' => '',
    '%drush-script' => '',
  ),
);
